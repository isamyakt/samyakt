+++
+++

## Intro

 ```zsh
 📂 ~/code  
❯❯❯ cat ~/whoami.md                                                        
──────┬─────────────────────────────────────────────────────────────
       │ File: /home/samyakt/whoami.md
──────┼─────────────────────────────────────────────────────────────
   1   │ # Who Am I ?
   2   │ 
   3   │ Myself Samyakt, Im passionate Rust developer interested
   4   │ in tackling challenges within the realm of decentralized
   5   │ finance.
   6   │ 
   7   │ ## My Commitment
   8   │ 
   9   │ - `git commit` is the my biggest commitment
──────┴─────────────────────────────────────────────────────────────
 📂 ~/code  
❯❯❯
```

## Blog Posts

Explore insightful blog posts on a variety of topics:

- 🧑‍💻 [How Data Structure perform operations?](./blog/my-first-blog)
- 🔖 [How to read and write JSON Data in Rust?](./blog/rust-json)
- 🏝 [Solana: Airdrops & Transactions using Rust](./blog/solana-airdrop-tx)
- 🔥 [Solana: Sending multiple transactions atomically using Rust](./blog/solana-multi-txs)

## Tags

Browse posts by tags:

- [rust](./tags/rust)
- [solana](./tags/solana)

## About Me

Stuff's I like:

  - [code](./about)
  - [read](./books)
  - [patriotic](https://open.spotify.com/playlist/5iOPYH6qXcCJKgDoIc0Gfv?si=def631010ba340d8)

## Online Presence

Stay connected with us:

- Email: [isamyakt17@gmail.com](mailto:isamyakt17@gmail.com)
- Code Repositories: [samyaktx@github](https://github.com/samyaktx)
- @isamyakt anywhere else.