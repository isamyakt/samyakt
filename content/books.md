+++
title = "Reading List"
date = 2023-10-05
draft = false
+++

## Technical Development
- ✔︎ [The Rust Programming Language](https://doc.rust-lang.org/book/)
- ✔︎ [Rust Atomics and Locks: Low-Level Concurrency in Practice](https://marabos.nl/atomics/)
- [ ] [The Little Book of Rust Macros](https://danielkeep.github.io/tlborm/book/index.html)
- [ ] [Hands-On Data Structures and Algorithms with Rust](https://www.packtpub.com/product/hands-on-data-structures-and-algorithms-with-rust/9781788995528)
- [ ] [Designing Data-Intensive Applications](https://raw.githubusercontent.com/ms2ag16/Books/master/Designing%20Data-Intensive%20Applications%20-%20Martin%20Kleppmann.pdf)
- [ ] [Database Internals](https://www.databass.dev)

## Personal Development
- ✔︎ Power of Subconscious Mind
- ✔︎ Atomic Habits
- ✔︎ Ikigai
- ✔︎ Think and Grow Rich